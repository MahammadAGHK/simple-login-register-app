You can find the deployed web app at http://13.49.77.141:9000/

visit http://13.49.77.141:9000/submit  to create a new profile

visit http://13.49.77.141:9000/login   to check if user exists or not to login

visit http://13.49.77.141:9000/  to list all profiles


Write a detailed README that explains how to set up your development environment, build the Docker image, run the application locally, and access the deployed application.

### first clone the repo:
- git clone https://gitlab.com/MahammadAGHK/simple-login-register-app
- cd simple-login-register-app

### then if not installed, install docker:

### my app uses mysql database so we need to run a mysql container using docker
- docker run -d -e MYSQL_ROOT_PASSWORD='$MYSQL_ROOT_PASSWORD' -p 3306:3306 --name mysql mysql:latest

here change MYSQL_ROOT_PASSWORD

after this command please wait 60 seconds for conatiner to be fully ready beacuse if you dont wait the next command can cause some errors

### then we need to initialize a database and a table structure for our app:
for this purpose I have created database.sql file which we will run it on mysql conatiner using the below command

- docker exec -i mysql mysql -u root -p'$DB_PASSWORD' < database.sql

here change DB_PASSWORD
$DB_PASSWORD=$MYSQL_ROOT_PASSWORD

if this command return some kind of error please run it again a few times because maybe mysql container isn't fully ready

### Then simply build the app image:
- docker build -t app:latest .

You can look at Dockerfile to see all docker commands of how to build my app

### here if you want you can test the app by running the below command:
- docker run -d -e DB_HOST=mysql -e DB_USER=root -e DB_PASSWORD='$DB_PASSWORD' -e DB_NAME=express_db -p 9000:9000 --link mysql:3306 --name app-test app:latest npm run test

these environment variable can also be specified in .env file I have put .env.example in the repo please change the above environment variable DB_PASSWORD
$DB_PASSWORD=$MYSQL_ROOT_PASSWORD

this will generate a report under mochawesome-report folder named as mochawesome.html
- docker logs app-test

you can look at test results from the logs

- docker cp app-test:/usr/src/app/mochawesome-report .

this copies report folder to your machine and then you can visit mochawesome-report/mochawesome.html or mochawesome-report/mochawesome.json to see the report of test results


### Then run the app container using this command:
- docker run -d -e DB_HOST=mysql -e DB_USER=root -e DB_PASSWORD='$DB_PASSWORD' -e DB_NAME=express_db -p 9000:9000 --link mysql:3306 --name app app:latest

these environment variable can also be specified in .env file I have put .env.example in the repo please change the above environment variable DB_PASSWORD

### AND you can access the app at http://localhost:9000 here 
you can go to http://localhost:9000/submit to create a new profile

or you can go to http://localhost:9000/login to check your credentials to check if a user exists or not
