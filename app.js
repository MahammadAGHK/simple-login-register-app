var express = require('express');

var app = express();

var path = require('path');

var mysql = require('mysql2');

var bodyParser = require('body-parser');

const { body,validationResult } = require('express-validator');

const bcrypt = require('bcrypt');

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: false}));

const dotenv = require('dotenv');
dotenv.config();

var conn = mysql.createConnection({

    host: process.env.DB_HOST,

    user: process.env.DB_USER,

    password: process.env.DB_PASSWORD,

    database: process.env.DB_NAME

});

conn.connect((err) => {
    if (err) throw err;
});

var message ="";
var obj = {};  
var valid = [];

app.get('/', (req, res) => {

        conn.query("SELECT * FROM users", function(err, result){

            if (err) throw err;

            else{
                obj = result;

                res.render('index', {obj: obj});

            }
        })
});
app.get('/submit', (req, res) => {

    res.render('submit', { message: message, valid: valid});

})
app.get('/login', (req,res) => {
    res.render('login', {message: message});
})
app.post('/login',(req,res)=>{
    username = req.body.username;
    password = req.body.password;
    console.log(username);
    conn.query("select * from users where username = ? limit 1",[username],function(err,result){
        if(err) throw err;
        if (result.length){
            bcrypt.compare(password, result[0].password).then(function(output) {
                if (output){
                    message="You successfully logined";
                    res.render('login', {message: message});
                }
                else{
                    message="username or password incorrect";
                    res.render('login', {message: message});
                }
            });
        }
        else{
            message="username or password incorrect";
            res.render('login', {message: message});
        }
        

    })
})
app.post('/submit', 
    body("name").trim().escape().isLength({min: 3, max: 15}).withMessage("Name should be minimum 3, maximum 15 characters"),
    body("surname").trim().escape().isLength({min: 3, max: 15}).withMessage("Surname should be minimum 3, maximum 15 characters"),
    body("age").matches('^[0-9]+$').withMessage("age should contain only numbers"),
    body("username").trim().escape().isLength({min: 3, max: 15}).withMessage("Username should be minimum 3, maximum 15 characters"),
    body("password").isLength({min: 8}).withMessage("password should be minimum 8 characters")
        .matches('[0-9]').withMessage("password should contain at least one number")
        .matches('[A-Z]').withMessage("password should contain at least one capital letter"),
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()){
            res.render('submit', { message: message, valid: errors.array()});
        }
        else{
            bcrypt.hash(req.body.password, 10).then(function(hashedpass) {
                let successMessage = "Record successfullly submitted to database!";

                let sql = `INSERT INTO users (name, surname, age, username, password) VALUES (?, ?, ?, ?, ?)`;

                conn.query(sql, [req.body.name, req.body.surname, req.body.age, req.body.username, hashedpass], function(err, result){

                    if(err) throw err;

                    console.log("one record has been inserted!");      

                    res.render('submit', { message: successMessage, valid: valid });
                });
            });
        }
});
const server = app.listen(9000,()=>{
    console.log("Server is running");
});

module.exports = server;